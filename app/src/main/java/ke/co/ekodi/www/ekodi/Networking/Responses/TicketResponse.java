package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TicketResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Tickets")
    @Expose
    private List<Ticket> tickets = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;

    }
}
