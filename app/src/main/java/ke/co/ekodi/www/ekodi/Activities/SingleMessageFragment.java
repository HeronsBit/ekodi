package ke.co.ekodi.www.ekodi.Activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleMessageFragment extends Fragment {

    TextView date, message;

    public SingleMessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_single_message, container, false);
        date = view.findViewById(R.id.tvChatDate);
        message= view.findViewById(R.id.tvChatMessage);

//        String gettingChatMessage = TokenPrefUtil.getTenantChatMessage(getContext());
//        String gettingChatDate = TokenPrefUtil.getTenantChatDate(getContext());
//
//        String[] messages = gettingChatMessage.split(",");
//        String[] dates = gettingChatDate.split(",");
//
//        message.setText(gettingChatMessage);
//        date.setText(gettingChatDate);
        // Inflate the layout for this fragment
        return view;
    }

}
