package ke.co.ekodi.www.ekodi.Activities;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.Networking.Responses.PssResetResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.GenMessages;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ResetPassword extends Fragment {

    EditText email;
    Button reset;
    TextView cancel,back;
    LoginFragment.OnLoginFormActivityListener onLoginFormActivityListener;
    private ApiEndPoints apiEndPoints;
    ProgressBar passReset;
    FragmentActivity activity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reset_password, container, false);
        setHasOptionsMenu(true);
        email = view.findViewById(R.id.reset_email);
        reset = view.findViewById(R.id.buttonReset);
        cancel = view.findViewById(R.id.resetCancel);
        passReset = view.findViewById(R.id.reset_progress);
        back = view.findViewById(R.id.backLogin);
        activity = getActivity();

        passReset.setVisibility(View.GONE);

        apiEndPoints = ApiUtils.getConfig();

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performReset();


            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                performPassReset();
                onLoginFormActivityListener.passResetCancel();

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLoginFormActivityListener.passResetCancel();
            }
        });

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;

        onLoginFormActivityListener = (LoginFragment.OnLoginFormActivityListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void performReset() {

        if (!valid()){
            return;
        }

        passReset.setVisibility(View.VISIBLE);
        String acc = email.getText().toString();

        apiEndPoints.performPasswordReset(acc).enqueue(new Callback<PssResetResponse>() {
            @Override
            public void onResponse(Call<PssResetResponse> call, Response<PssResetResponse> response) {

                if (response.body().getStatus() == 200) {
                    Log.d("MESSAGELOG",response.body().getMessage());
                    GenMessages.makeToastFragment(activity,getContext(),response.body().getMessage());
//                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    onLoginFormActivityListener.passResetCancel();

                }else {
                    passReset.setVisibility(View.GONE);
                    GenMessages.makeToastFragment(activity,getContext(),"Network Error");
//                    Toast.makeText(getContext(),"Network Error",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<PssResetResponse> call, Throwable t) {
                t.printStackTrace();
                passReset.setVisibility(View.GONE);
                GenMessages.makeToastFragment(activity,getContext(),"Check your internet connection and try again");
//                Toast.makeText(getContext(), "There is a password reset problem", Toast.LENGTH_LONG).show();
            }
        });

//        passReset.setVisibility(View.GONE);


    }

    private boolean valid() {
        boolean valid = true;
        String mail = email.getText().toString();

        if (mail.isEmpty()) {
            email.setError("username required");
            valid = false;
        } else {
            email.setError(null);
        }

        return valid;
    }

}
