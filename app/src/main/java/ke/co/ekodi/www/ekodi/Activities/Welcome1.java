package ke.co.ekodi.www.ekodi.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.PreferenceManager;

public class Welcome1 extends AppCompatActivity {

    Button buttonDo;
    TextView tv1,tv2,tvSkip;
    public PreferenceManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefManager = new PreferenceManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }
            //making notification bar transparent

            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            }

            setContentView(R.layout.activity_welcome1);

            buttonDo = findViewById(R.id.buttonDo);

            tv1 = findViewById(R.id.textView);
            tv2 = findViewById(R.id.textView2);
            tvSkip = findViewById(R.id.tvSkip);

            Typeface myFont = Typeface.createFromAsset(getAssets(), "fonts/Seravek-Bold.ttf");
            Typeface myFontThin = Typeface.createFromAsset(getAssets(), "fonts/Seravek-Light.ttf");
            tv1.setTypeface(myFont);
            tv2.setTypeface(myFontThin);
            tvSkip.setTypeface(myFont);

            buttonDo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent welcome = new Intent(Welcome1.this, Welcome.class);
                    startActivity(welcome);
                    finish();

                }
            });

            tvSkip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    launchHomeScreen();
                    finish();
                }
            });

    }


    public void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(Welcome1.this, Dashboard.class));
        finish();
    }
}
