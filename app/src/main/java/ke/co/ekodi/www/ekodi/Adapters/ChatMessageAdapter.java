package ke.co.ekodi.www.ekodi.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ke.co.ekodi.www.ekodi.Models.ChatMessageModel;
import ke.co.ekodi.www.ekodi.Models.InvoiceModel;
import ke.co.ekodi.www.ekodi.R;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.MyViewHolder> {

    private List<ChatMessageModel> chatMessages;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView message,date;
        View layout;
        public MyViewHolder(View v) {
            super(v);
            layout = v;
            message = v.findViewById(R.id.chat_message_text);
            date = v.findViewById(R.id.chat_date_text);
        }
    }

    public void add(int position, ChatMessageModel item) {
        chatMessages.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        chatMessages.remove(position);
        notifyItemRemoved(position);
    }

    public ChatMessageAdapter(List<ChatMessageModel> chatMessages) {
        this.chatMessages = chatMessages;
    }

    @NonNull
    @Override
    public ChatMessageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(
                viewGroup.getContext());
        View v = inflater.inflate(R.layout.fragment_single_message_row, viewGroup, false);

        ChatMessageAdapter.MyViewHolder vh = new ChatMessageAdapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatMessageAdapter.MyViewHolder myViewHolder, int i) {

        final String msg = chatMessages.get(i).getMessage();
        final String dt = chatMessages.get(i).getDate();
        myViewHolder.message.setText(msg);
        myViewHolder.date.setText(dt);
    }

    @Override
    public int getItemCount() {
        return chatMessages.size();
    }
}
