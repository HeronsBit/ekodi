package ke.co.ekodi.www.ekodi.Activities;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Adapters.MessageAdapter;
import ke.co.ekodi.www.ekodi.Adapters.TransactionAdapter;
import ke.co.ekodi.www.ekodi.Models.MessageModel;
import ke.co.ekodi.www.ekodi.Models.TicketModel;
import ke.co.ekodi.www.ekodi.Models.TransactionModel;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;

public class MessagesFragment extends Fragment {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public MessagesFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_messages, container, false);

        mRecyclerView = view.findViewById(R.id.message_recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<MessageModel> input = new ArrayList<>();

        String gettingChatMessage = TokenPrefUtil.getTenantChatMessage(getContext());
        String gettingChatDate = TokenPrefUtil.getTenantChatDate(getContext());

        String[] message = gettingChatMessage.split(",");
//        for (int j=0;j<message.length;j++){
//            Log.d("Message"+(j+1),message[j]);
//        }
//        Log.isLoggable("length",message.length);
        String[] dates = gettingChatDate.split(",");
        for (int i=0;i<message.length;i++){
            input.add(new MessageModel("MESSAGE "+(i+1),dates[i],message[i]));
        }

        mAdapter = new MessageAdapter(input,getContext());
        mRecyclerView.setAdapter(mAdapter);
        
        return view;
    }
}
