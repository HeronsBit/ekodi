package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopUpDetails {

    @SerializedName("mpesa_paybill")
    @Expose
    private String mpesaPaybill;
    @SerializedName("bank_branch")
    @Expose
    private String bankBranch;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_account_name")
    @Expose
    private String bankAccountName;
    @SerializedName("airtel_paybill")
    @Expose
    private String airtelPaybill;

    public String getMpesaPaybill() {
        return mpesaPaybill;
    }

    public void setMpesaPaybill(String mpesaPaybill) {
        this.mpesaPaybill = mpesaPaybill;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getAirtelPaybill() {
        return airtelPaybill;
    }

    public void setAirtelPaybill(String airtelPaybill) {
        this.airtelPaybill = airtelPaybill;
    }

}
