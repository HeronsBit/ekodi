package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TenantPropertyDetails {

    @SerializedName("unit_name")
    @Expose
    private String unitName;
    @SerializedName("unit_type")
    @Expose
    private String unitType;
    @SerializedName("unit_rent")
    @Expose
    private String unitRent;
    @SerializedName("building_name")
    @Expose
    private String buildingName;

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getUnitRent() {
        return unitRent;
    }

    public void setUnitRent(String unitRent) {
        this.unitRent = unitRent;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }
}
