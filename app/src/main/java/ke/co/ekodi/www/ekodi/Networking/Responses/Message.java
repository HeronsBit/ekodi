package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Message {

    @SerializedName("tenant_id")
    @Expose
    private String tenantId;
    @SerializedName("Full_names")
    @Expose
    private String fullNames;
    @SerializedName("occupation")
    @Expose
    private Object occupation;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("Phone_Number")
    @Expose
    private String phoneNumber;
    @SerializedName("sex")
    @Expose
    private String sex;
    @SerializedName("id_no")
    @Expose
    private String idNo;
    @SerializedName("kra_pin")
    @Expose
    private String kraPin;
    @SerializedName("house_no")
    @Expose
    private String houseNo;
    @SerializedName("property_id")
    @Expose
    private String propertyId;
    @SerializedName("property_allocated")
    @Expose
    private String propertyAllocated;
    @SerializedName("tenant_from")
    @Expose
    private String tenantFrom;
    @SerializedName("lease_period")
    @Expose
    private Object leasePeriod;
    @SerializedName("rent_per_month")
    @Expose
    private Object rentPerMonth;
    @SerializedName("image_id")
    @Expose
    private Object imageId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("landlord_id")
    @Expose
    private String landlordId;
    @SerializedName("property_house_no")
    @Expose
    private String propertyHouseNo;
    @SerializedName("agreement_file")
    @Expose
    private String agreementFile;
    @SerializedName("tenant_acc_no")
    @Expose
    private String tenantAccNo;
    @SerializedName("end_of_lease")
    @Expose
    private Object endOfLease;
    @SerializedName("kin_name")
    @Expose
    private String kinName;
    @SerializedName("kin_phone")
    @Expose
    private String kinPhone;
    @SerializedName("wizard")
    @Expose
    private String wizard;
    @SerializedName("prev_property")
    @Expose
    private Object prevProperty;
    @SerializedName("prev_unit")
    @Expose
    private Object prevUnit;

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getFullNames() {
        return fullNames;
    }

    public void setFullNames(String fullNames) {
        this.fullNames = fullNames;
    }

    public Object getOccupation() {
        return occupation;
    }

    public void setOccupation(Object occupation) {
        this.occupation = occupation;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getKraPin() {
        return kraPin;
    }

    public void setKraPin(String kraPin) {
        this.kraPin = kraPin;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyAllocated() {
        return propertyAllocated;
    }

    public void setPropertyAllocated(String propertyAllocated) {
        this.propertyAllocated = propertyAllocated;
    }

    public String getTenantFrom() {
        return tenantFrom;
    }

    public void setTenantFrom(String tenantFrom) {
        this.tenantFrom = tenantFrom;
    }

    public Object getLeasePeriod() {
        return leasePeriod;
    }

    public void setLeasePeriod(Object leasePeriod) {
        this.leasePeriod = leasePeriod;
    }

    public Object getRentPerMonth() {
        return rentPerMonth;
    }

    public void setRentPerMonth(Object rentPerMonth) {
        this.rentPerMonth = rentPerMonth;
    }

    public Object getImageId() {
        return imageId;
    }

    public void setImageId(Object imageId) {
        this.imageId = imageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLandlordId() {
        return landlordId;
    }

    public void setLandlordId(String landlordId) {
        this.landlordId = landlordId;
    }

    public String getPropertyHouseNo() {
        return propertyHouseNo;
    }

    public void setPropertyHouseNo(String propertyHouseNo) {
        this.propertyHouseNo = propertyHouseNo;
    }

    public String getAgreementFile() {
        return agreementFile;
    }

    public void setAgreementFile(String agreementFile) {
        this.agreementFile = agreementFile;
    }

    public String getTenantAccNo() {
        return tenantAccNo;
    }

    public void setTenantAccNo(String tenantAccNo) {
        this.tenantAccNo = tenantAccNo;
    }

    public Object getEndOfLease() {
        return endOfLease;
    }

    public void setEndOfLease(Object endOfLease) {
        this.endOfLease = endOfLease;
    }

    public String getKinName() {
        return kinName;
    }

    public void setKinName(String kinName) {
        this.kinName = kinName;
    }

    public String getKinPhone() {
        return kinPhone;
    }

    public void setKinPhone(String kinPhone) {
        this.kinPhone = kinPhone;
    }

    public String getWizard() {
        return wizard;
    }

    public void setWizard(String wizard) {
        this.wizard = wizard;
    }

    public Object getPrevProperty() {
        return prevProperty;
    }

    public void setPrevProperty(Object prevProperty) {
        this.prevProperty = prevProperty;
    }

    public Object getPrevUnit() {
        return prevUnit;
    }

    public void setPrevUnit(Object prevUnit) {
        this.prevUnit = prevUnit;
    }

}
