package ke.co.ekodi.www.ekodi.Networking;

import ke.co.ekodi.www.ekodi.AppController;

public class ApiUtils {

    public static ApiEndPoints getConfig(){
        return ApiClient.getClient(AppController.getmInstance().getApplicationContext(),Urls.BASE_URL).create(ApiEndPoints.class);
    }

}
