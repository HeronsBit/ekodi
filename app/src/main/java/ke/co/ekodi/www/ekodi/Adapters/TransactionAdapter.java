package ke.co.ekodi.www.ekodi.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ke.co.ekodi.www.ekodi.Models.InvoiceModel;
import ke.co.ekodi.www.ekodi.Models.TransactionModel;
import ke.co.ekodi.www.ekodi.R;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {


    private List<TransactionModel> transactionData;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title,subTitle,amount;
        View layout;
        public MyViewHolder(View v) {
            super(v);
            layout = v;
            title = v.findViewById(R.id.transaction_title);
            subTitle = v.findViewById(R.id.transaction_sub_title);
            amount = v.findViewById(R.id.transaction_amount);
        }
    }

    public void add(int position, TransactionModel item) {
        transactionData.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        transactionData.remove(position);
        notifyItemRemoved(position);
    }

    public TransactionAdapter(List<TransactionModel> transactionData) {
        this.transactionData = transactionData;
    }

    @NonNull
    @Override
    public TransactionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(
                viewGroup.getContext());
        View v = inflater.inflate(R.layout.fragment_single_transaction, viewGroup, false);

        TransactionAdapter.MyViewHolder vh = new TransactionAdapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionAdapter.MyViewHolder myViewHolder, int i) {

        final String transTitle = transactionData.get(i).getTitle();
        final String transSubTitle = transactionData.get(i).getSubTitle();
        final String transAmount = transactionData.get(i).getAmount();
        myViewHolder.title.setText(transTitle);
        myViewHolder.subTitle.setText(transSubTitle);
        myViewHolder.amount.setText(transAmount);

    }

    @Override
    public int getItemCount() {
        return transactionData.size();
    }
}
