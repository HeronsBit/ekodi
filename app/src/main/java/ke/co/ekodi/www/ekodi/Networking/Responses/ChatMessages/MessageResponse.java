package ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import ke.co.ekodi.www.ekodi.Networking.Responses.Message;

public class MessageResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Message")
    @Expose
    private List<ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.Message> message = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.Message> getMessage() {
        return message;
    }

    public void setMessage(List<ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.Message> message) {
        this.message = message;
    }

}
