package ke.co.ekodi.www.ekodi.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ke.co.ekodi.www.ekodi.Models.InvoiceModel;
import ke.co.ekodi.www.ekodi.Models.TicketModel;
import ke.co.ekodi.www.ekodi.R;

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.MyViewHolder> {

    private List<TicketModel> ticketData;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title,date,status;
        View layout;
        public MyViewHolder(View v) {
            super(v);
            layout = v;
            title = v.findViewById(R.id.ticket_title);
            date = v.findViewById(R.id.ticket_date);
            status = v.findViewById(R.id.ticket_status);
        }
    }

    public void add(int position, TicketModel item) {
        ticketData.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        ticketData.remove(position);
        notifyItemRemoved(position);
    }

    public TicketAdapter(List<TicketModel> ticketData) {
        this.ticketData = ticketData;
    }

    @NonNull
    @Override
    public TicketAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(
                viewGroup.getContext());
        View v = inflater.inflate(R.layout.fragment_ticket_row, viewGroup, false);

        TicketAdapter.MyViewHolder vh = new TicketAdapter.MyViewHolder(v);

        return vh;

    }

    @Override
    public void onBindViewHolder(@NonNull TicketAdapter.MyViewHolder myViewHolder, int i) {

        final String ticketTitle = ticketData.get(i).getTitle();
        final String ticketDate = ticketData.get(i).getDate();
        final String ticketStatus = ticketData.get(i).getStatus();
        myViewHolder.title.setText(ticketTitle);
        myViewHolder.date.setText(ticketDate);
        myViewHolder.status.setText(ticketStatus);

    }

    @Override
    public int getItemCount() {
        return ticketData.size();
    }
}
