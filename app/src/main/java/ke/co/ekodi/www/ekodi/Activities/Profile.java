package ke.co.ekodi.www.ekodi.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toolbar;

import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;

public class Profile extends AppCompatActivity {
    TextView name,phone,email,dob;
//    Button test;
    BottomNavigationView bottomNavigationView;
    android.support.v7.widget.Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        name = findViewById(R.id.full_name);
        phone = findViewById(R.id.user_phone);
        email = findViewById(R.id.user_email);
        dob = findViewById(R.id.dob);
        bottomNavigationView = findViewById(R.id.profileBottom);
//        test = findViewById(R.id.test);
        bottomNavigationView.setSelectedItemId(R.id.navigation_profile);
        name.setText(TokenPrefUtil.getName(getApplicationContext()));
        phone.setText(TokenPrefUtil.getPhone(getApplicationContext()));
        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        email.setText(TokenPrefUtil.getEmail(getApplicationContext()));
        dob.setText(TokenPrefUtil.getDob(getApplicationContext()));

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle("MY PROFILE");
            getSupportActionBar().setSubtitle(null);
        }

//        test.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(getApplicationContext(), Test.class);
//                startActivity(i);
//            }
//        });


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_dash:
                        Intent dashIntent = new Intent(Profile.this, Dashboard.class);
//                dashIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                        finish();
                        startActivity(dashIntent);
                        overridePendingTransition(R.anim.fromright,R.anim.fromleft);
                        return true;
                    case R.id.navigation_finance:
                        Intent finIntent = new Intent(Profile.this, Finance.class);
                        finIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                        finish();
                        startActivity(finIntent);
                        overridePendingTransition(R.anim.fromright,R.anim.fromleft);
                        return true;
                    case R.id.navigation_interactions:
                        Intent intent = new Intent(Profile.this, Interactions.class);
                        intent.putExtra("fragmentTo", R.layout.fragment_interactions);
                        finish();
                        startActivity(intent);
                        overridePendingTransition(R.anim.fromright, R.anim.fromleft);
                        return  true;
                    default:
                        return true;
                }

            }
        });



    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()){
//            case android.R.id.home:
//                Intent intent = new Intent(this, Finance.class);
//                intent.putExtra("fragmentTo", R.layout.fragment_finance);
//                finish();
//                startActivity(intent);
//                overridePendingTransition(R.anim.fromright,R.anim.fromright);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }
}
