package ke.co.ekodi.www.ekodi.Activities;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.R;


public class InteractionsFragment extends Fragment {

    FloatingActionButton myFloatingButton;
    Button add_ticket;
    Animation fabOpen, fabClose, fabClockwise, fabAnticlockwise;
    boolean isOpen = false;
    onInteractionsFragmentListener interactionsFragmentListener;

    public interface onInteractionsFragmentListener{
        public void showNoticeBoard(View view);
        public void loadTickets(View view);
        public  void loadMessages(View view);
        public  void viewChats(View view);
        public  void ticketChats(View view);
        public  void loadTicketAdder(View view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_interactions, container, false);
        myFloatingButton = view.findViewById(R.id.ticketFloatingActionButton);
        add_ticket = view.findViewById(R.id.add_ticket_button);

        fabOpen = AnimationUtils.loadAnimation(getContext(), R.anim.fab_open);
        fabClose = AnimationUtils.loadAnimation(getContext(), R.anim.fab_close);
        fabClockwise = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_clockwise);
        fabAnticlockwise = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_anticlockwise);

        myFloatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOpen){

                    add_ticket.startAnimation(fabClose);
                    myFloatingButton.startAnimation(fabAnticlockwise);
                    add_ticket.setClickable(false);
                    isOpen=false;

                }else {
                    add_ticket.startAnimation(fabOpen);
                    myFloatingButton.startAnimation(fabClockwise);
                    add_ticket.setClickable(true);
                    isOpen=true;

                }
            }
        });
        // Inflate the layout for this fragment
        return view;
    }



}
