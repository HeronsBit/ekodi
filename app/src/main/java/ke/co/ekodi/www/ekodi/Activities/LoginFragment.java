package ke.co.ekodi.www.ekodi.Activities;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.Networking.Responses.LoginResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.Message;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantInvoicesResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantPropertyDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TopUpDetailsResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.GenMessages;
import ke.co.ekodi.www.ekodi.Utilities.PreferenceManager;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginFragment extends Fragment {

    EditText email, password;
    Button logIn,proceed;
    TextView forgottPass;
    OnLoginFormActivityListener onLoginFormActivityListener;
    Dashboard dash;
    ProgressBar loginProgress;
    FragmentActivity activity;

    private ApiEndPoints ApiService;


    public interface OnLoginFormActivityListener {
        public void passResetCancel();

        public void doLogin();

        public void doPassReset();
        public void finish();

        public void doNetwokingStuff();
        public void refreshDashboard();
        public void refreshFragment();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
      menu.clear();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        setHasOptionsMenu(true);
        ApiService = ApiUtils.getConfig();

        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        logIn = view.findViewById(R.id.buttonSignin);
        proceed = view.findViewById(R.id.proceed);
        forgottPass = view.findViewById(R.id.pass_forgot);
        loginProgress = view.findViewById(R.id.login_progress);
        loginProgress.setVisibility(View.GONE);
        activity = getActivity();
//        loginProgress.getIndeterminateDrawable().setColorFilter(#073855,PorterDuff.Mode.MULTIPLY);

        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginProgress.setVisibility(View.VISIBLE);

                performLogin();

//                GenMessages.makeToast(activity,getContext(),"Credentials Check Successful");
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLoginFormActivityListener.doLogin();
                GenMessages.makeToastFragment(activity,getContext(),"Login successful");
            }
        });

        forgottPass.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                onLoginFormActivityListener.doPassReset();

            }
        });
        return view;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                // todo: goto back activity from here
                onLoginFormActivityListener.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        Activity activity = (Activity) context;

        onLoginFormActivityListener = (OnLoginFormActivityListener) activity;

    }
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Log.i("IsRefresh", "Yes");
        }
    }

    public void performLogin() {

        if (!valid()) {
            loginProgress.setVisibility(View.GONE);
            return;
        }

        logIn.setText(R.string.checking);

        String mail = email.getText().toString();
        String pass = password.getText().toString();

        TokenPrefUtil.storeAccNo(getContext(),mail);

        ApiService.performLogin(mail,pass).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                assert response.body() != null;
                if (response.body().getStatus() == 200){
                    if (response.body().getMessage().equals("Wrong password.")){
                        GenMessages.makeToastFragment(activity,getContext(),"You entered a wrong password");
//                        Toast.makeText(getContext(),"You entered a wrong password",Toast.LENGTH_LONG).show();
                        logIn.setText(R.string.sign_in);
                        loginProgress.setVisibility(View.GONE);
                    }else if (response.body().getMessage().equals("Successful login.")){
//                    spinner.setVisibility(View.VISIBLE);
                        TokenPrefUtil.storeApiKey(getContext(), response.body().getToken());
                        TokenPrefUtil.storeUserId(getContext(), response.body().getUserId());
//                        GenMessages.makeToastFragment(activity,getContext(),response.body().getMessage());
//                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        PreferenceManager pref = new PreferenceManager(getContext());
                        pref.setToken(response.body().getToken());
                        pref.setLoginStatus(true);

                        String token = TokenPrefUtil.getApiKey(getContext());
                        String userId = TokenPrefUtil.getUserId(getContext());
                        String id = TokenPrefUtil.getId(getContext());

//                    Log.d("TOKEN",token);
//                    Log.d("User-ID",userId);

                        logIn.setClickable(false);

                        onLoginFormActivityListener.doNetwokingStuff();
//                        onLoginFormActivityListener.doLogin();
//                        onLoginFormActivityListener.refreshFragment();
                        loginProgress.setVisibility(View.GONE);
                        GenMessages.makeToastFragment(activity,getContext(),"Validation Successful");
                        logIn.setVisibility(View.INVISIBLE);
                        proceed.setVisibility(View.VISIBLE);

                    }else {
                        GenMessages.makeToastFragment(activity,getContext(),"Unknown Username");
//                        Toast.makeText(getContext(),"Unknown Username",Toast.LENGTH_LONG).show();
                        logIn.setText(R.string.sign_in);
                        loginProgress.setVisibility(View.GONE);
                    }

                }else {
                    //TODO:: inpform the user that login not successiful
                    logIn.setText(R.string.sign_in);
                    GenMessages.makeToastFragment(activity,getContext(),"Unknown Error Occurred");
//                    Toast.makeText(getContext(),"Unknown Error Occurred",Toast.LENGTH_LONG).show();
                    loginProgress.setVisibility(View.GONE);
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                t.printStackTrace();

                GenMessages.makeToastFragment(activity,getContext(),"No internet Or your username is wrong");
//                Toast.makeText(getContext(),"No internet Or your username is wrong",Toast.LENGTH_LONG).show();
                logIn.setText(R.string.sign_in);
                loginProgress.setVisibility(View.GONE);

            }
        });

    }


    private boolean valid() {
        boolean valid = true;
        String mail = email.getText().toString();
        String pass = password.getText().toString();

        if (mail.isEmpty()) {
            email.setError("username required");
            valid = false;
        } else if (pass.isEmpty()) {
            password.setError("Password field required");
            valid = false;
        } else if (pass.length() < 5) {
            password.setError("password too short");
            valid = false;
        } else {
            email.setError(null);
            password.setError(null);
        }

        return valid;
    }

}
