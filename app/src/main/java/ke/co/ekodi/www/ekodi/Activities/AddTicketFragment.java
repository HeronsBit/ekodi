package ke.co.ekodi.www.ekodi.Activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.Networking.Responses.AddTicketResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.Ticket;
import ke.co.ekodi.www.ekodi.Networking.Responses.TicketResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.GenMessages;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddTicketFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private Spinner spinnerCat;
    private Spinner spinnerLev;
    private Button submitTicket;
    EditText sub, message;
    private ApiEndPoints apiService;
    ProgressBar ticketProg;
    private static final String[] categoris = {"Select Category", "Utility"};
    private static final String[] levels = {"Select Level","Low", "High"};
    OnTicketFormActivityListener interactionsFragmentListener;
    FragmentActivity activity;

    public AddTicketFragment() {

        // Required empty public constructor
    }

    public interface OnTicketFormActivityListener {

        public  void  updateAllData();

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_ticket, container, false);
        spinnerCat = view.findViewById(R.id.category_spinner);
        spinnerLev = view.findViewById(R.id.level_spinner);
        submitTicket = view.findViewById(R.id.button_submit_ticket);
        sub = view.findViewById(R.id.ticket_subject);
        message = view.findViewById(R.id.ticket_msg);
        ticketProg = view.findViewById(R.id.ticket_progress);
        ticketProg.setVisibility(View.GONE);
        apiService = ApiUtils.getConfig();
        activity=getActivity();

        ArrayAdapter<String> categoryAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item,categoris);


        ArrayAdapter<String> levelAdapter = new ArrayAdapter<String>(getContext(),
                android.R.layout.simple_spinner_item,levels);

        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCat.setAdapter(categoryAdapter);
//        spinnerCat.setOnItemSelectedListener();

        levelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLev.setAdapter(levelAdapter);

        submitTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!valid()){
                    return;
                }

                ticketProg.setVisibility(View.VISIBLE);

                String token = TokenPrefUtil.getApiKey(getContext());
                String user_id = TokenPrefUtil.getUserId(getContext());
                String acc_no = TokenPrefUtil.getUserId(getContext());
                String subject = sub.getText().toString();
                String category = spinnerCat.getSelectedItem().toString();
                String level = spinnerLev.getSelectedItem().toString();
                String mesg = message.getText().toString();

                apiService.createTicket(token,user_id,acc_no,subject,mesg,category,level).enqueue(new Callback<AddTicketResponse>() {
                    @Override
                    public void onResponse(Call<AddTicketResponse> call, Response<AddTicketResponse> response) {
                        try {
                            assert response.body() !=null;
                            if (response.body().getStatus() == 200){
                                Log.d("TICKET",response.body().getMessage());
                                storeTenantTickets();
//                                interactionsFragmentListener.updateAllData();
                                GenMessages.makeToastFragment(activity,getContext(),"New ticket created");

                                Intent intent = new Intent(getContext(),Dashboard.class);
                                startActivity(intent);
                                getActivity().finish();
//                                onLoginFormActivityListener.doNetwokingStuff();

                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddTicketResponse> call, Throwable t) {

                        t.printStackTrace();
                    }
                });

//                Toast.makeText(getContext(),"Submit",Toast.LENGTH_LONG).show();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void storeTenantTickets(){
        String token = TokenPrefUtil.getApiKey(getContext());
        String user_id = TokenPrefUtil.getUserId(getContext());
        String acc_no = TokenPrefUtil.getUserId(getContext());

        apiService.getTenantTickets(token,user_id,acc_no).enqueue(new Callback<TicketResponse>() {
            @Override
            public void onResponse(Call<TicketResponse> call, Response<TicketResponse> response) {
                try {
                    assert response.body() !=null;
                    if (response.body().getStatus() == 200){

                        List<Ticket> myTickets = response.body().getTickets();
                        List<String> status = new ArrayList<>();
                        List<String> date = new ArrayList<>();
                        List<String> title = new ArrayList<>();
                        for (int i=0;i<myTickets.size();i++){
                            String state = myTickets.get(i).getStatus();
                            String dt = myTickets.get(i).getDate();
                            String titl = myTickets.get(i).getTitle();
                            status.add(state);
                            date.add(dt);
                            title.add(titl);
                            Log.d("State",state);
                            Log.d("Title",titl);
                            Log.d("Date",dt);

                        }

                        TokenPrefUtil.storeTicketTitle(title);
                        TokenPrefUtil.storeTicketDate(date);
                        TokenPrefUtil.storeTicketState(status);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TicketResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }


    private boolean valid() {
        boolean valid = true;
        String subject = sub.getText().toString();
        String msg = message.getText().toString();
        String spCat = spinnerCat.getSelectedItem().toString();
        String spLev = spinnerLev.getSelectedItem().toString();

        if (subject.isEmpty()) {
            sub.setError("subject required");
            valid = false;
        } else if (spCat.equals("Select Category")) {
            Toast.makeText(getContext(), "Category Not Selected", Toast.LENGTH_SHORT).show();
            valid = false;
        } else if (spLev.equals("Select Level")) {
            Toast.makeText(getContext(), "Level Not Selected", Toast.LENGTH_SHORT).show();
            valid = false;
        }else if (msg.isEmpty()) {
            message.setError("message required");
            valid = false;
        } else {
            sub.setError(null);
            message.setError(null);
        }
        return valid;
    }
}
