package ke.co.ekodi.www.ekodi.Networking;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

import ke.co.ekodi.www.ekodi.Adapters.ItemTypeAdapterFactory;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ApiClient {

    private static Retrofit retrofit=null;

    private static OkHttpClient okHttpClient;

    private static final int REQUEST_TIMEOUT=60;

    public static Retrofit getClient(Context context, String baseUrl){

        if (okHttpClient==null)

            initOkHttp(context);

        if (retrofit==null){

            Gson gson=new GsonBuilder()

                    .setLenient()

                    .setPrettyPrinting()

                    .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                    .registerTypeAdapterFactory(new ItemTypeAdapterFactory())

                    .create();

            retrofit=new Retrofit.Builder()

                    .baseUrl(baseUrl)

                    .client(okHttpClient)

                    .addConverterFactory(GsonConverterFactory.create(gson))

                    .build();

        }

        return retrofit;

    }



    private static void initOkHttp(final Context context) {

        OkHttpClient.Builder httpClient=new OkHttpClient().newBuilder()

                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

                .readTimeout(REQUEST_TIMEOUT,TimeUnit.SECONDS)

                .writeTimeout(REQUEST_TIMEOUT,TimeUnit.SECONDS);

        HttpLoggingInterceptor interceptor=new HttpLoggingInterceptor();

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(interceptor);

        httpClient.addInterceptor(chain -> {

            Request original =chain.request();

            Request.Builder requestBuilder =original.newBuilder()

                    .addHeader("Client-Service","frontend-client")
                    .addHeader("Content-Type","application/x-www-form-urlencoded")
                    .addHeader("Mobile-Keys","Ekodi)*%^32KeY");



            ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.COMPATIBLE_TLS)
                    .tlsVersions(TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0)
                    .cipherSuites(
                            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                            CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256,
                            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA,
                            CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
                    .build();

            httpClient.connectionSpecs(Collections.singletonList(spec));


            //Adding Autholizationtoken(Api Key)

            //Requests will be denied without authorization token

            if (!TextUtils.isEmpty(TokenPrefUtil.getApiKey(context))) {

                requestBuilder.addHeader("Key", TokenPrefUtil.getApiKey(context));

            }



            Request request = requestBuilder.build();

            return chain.proceed(request);

        });

        okHttpClient =httpClient.build();



    }

}
