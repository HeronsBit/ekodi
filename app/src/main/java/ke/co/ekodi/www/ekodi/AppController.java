package ke.co.ekodi.www.ekodi;

import android.app.Application;

public class AppController extends Application {

    public static final String TAG =Application.class.getSimpleName();

    private static AppController mInstance;

    public static synchronized AppController getmInstance(){

        return mInstance;

    }


    @Override

    public void onCreate() {

        super.onCreate();

        mInstance=this;



    }
}
