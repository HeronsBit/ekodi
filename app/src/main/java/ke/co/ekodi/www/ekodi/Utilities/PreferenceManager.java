package ke.co.ekodi.www.ekodi.Utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import ke.co.ekodi.www.ekodi.R;

public class PreferenceManager {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    public Context _context;

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "intro_slider-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setLoginStatus( boolean status)
    {
        editor.putBoolean(_context.getString(R.string.pref_login_status),status);
        editor.commit();
    }

    public  boolean getLoginStatus(){
        return pref.getBoolean(_context.getString(R.string.pref_login_status),false);
    }

    public void setToken(String token){
        editor.putString(_context.getString(    R.string.token),token);
        editor.commit();
    }

    public String getToken(){
        return pref.getString(_context.getString(R.string.token),"token");
    }

    public void setName(String name){
        editor.putString(_context.getString(R.string.pref_user_name),name);
        editor.commit();

    }

    public String getUserName(){
        return pref.getString(_context.getString(R.string.pref_user_name),"User");
    }

    public void showToast(String message){
        Toast.makeText(_context, message, Toast.LENGTH_SHORT).show();
    }

}
