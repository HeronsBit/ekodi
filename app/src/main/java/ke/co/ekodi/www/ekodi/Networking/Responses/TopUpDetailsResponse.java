package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopUpDetailsResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Top Up Details")
    @Expose
    private TopUpDetails topUpDetails;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public TopUpDetails getTopUpDetails() {
        return topUpDetails;
    }

    public void setTopUpDetails(TopUpDetails topUpDetails) {
        this.topUpDetails = topUpDetails;
    }


}
