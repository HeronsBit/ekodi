package ke.co.ekodi.www.ekodi.Models;

public class TransactionModel {

    public  String title;
    public String subTitle;
    public String amount;

    public TransactionModel(String title, String subTitle, String amount) {
        this.title = title;
        this.subTitle = subTitle;
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
