package ke.co.ekodi.www.ekodi.Networking;

import ke.co.ekodi.www.ekodi.Networking.Responses.AddTicketResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.MessageResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.LoginResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.LogoutResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.ManagerDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.PssResetResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantInvoicesResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantPropertyDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TicketResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TopUpDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TransactionResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiEndPoints {

    @FormUrlEncoded
    @POST("MobileAuth/login")
    Call<LoginResponse> performLogin(
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("MobileAuth/reset_password")
    Call<PssResetResponse> performPasswordReset(
            @Field("account_number") String username
    );

    @FormUrlEncoded
    @POST("MobileApp/create_support_ticket")
    Call<AddTicketResponse> createTicket(
            @Header("token") String token,
            @Header("User-ID") String user_id,
            @Field("acc_no") String account,
            @Field("subject") String subject,
            @Field("message") String message,
            @Field("category") String category,
            @Field("level") String level
    );

//    @GET("MobileApp/tenant_details")
//    Call<TenantDetailsResponse> getTenantDetails(@Header("Client-Service") String h1,
//                                                 @Header("Content-Type") String h2, @Header("Mobile-Keys") String h3,
//                                                 @Header("token") String token, @Header("User-ID") String user_id,
//                                                 @Query("acc_no") String account_number
//    );


    @GET("MobileApp/tenant_details")
    Call<TenantDetailsResponse> getTenantDetails(@Header("token") String token,
                                                 @Header("User-ID") String user_id,
                                                 @Query("acc_no") String account_number
    );

    @POST("MobileAuth/logout")
    Call<LogoutResponse> logoutTenant(@Header("token") String token,
                                          @Header("User-ID") String user_id
    );

    @GET("MobileApp/tenant_property_details")
    Call<TenantPropertyDetailsResponse> getTenantPropertyDetails(@Header("token") String token,
                                                         @Header("User-ID") String user_id,
                                                         @Query("acc_no") String account_number
    );

    @GET("MobileApp/tenant_manager_details")
    Call<ManagerDetailsResponse> getMnagerDetails(@Header("Client-Service") String h1,
                                                  @Header("Content-Type") String h2, @Header("Mobile-Keys") String h3,
                                                  @Header("token") String token, @Header("User-ID") String user_id,
                                                  @Query("acc_no") String account_number
    );


    @GET("MobileApp/topup_details")
    Call<TopUpDetailsResponse> getTopUpDetails(@Header("token") String token,
                                               @Header("User-ID") String user_id,
                                               @Query("acc_no") String account_number
    );

    @GET("MobileApp/tenant_invoices")
    Call<TenantInvoicesResponse> getTenantInvoices(@Header("token") String token,
                                                   @Header("User-ID") String user_id,
                                                   @Query("acc_no") String account_number
    );

    @GET("MobileApp/tenant_transactions")
    Call<TransactionResponse> getTenantTransactions(@Header("token") String token,
                                                    @Header("User-ID") String user_id,
                                                    @Query("acc_no") String account_number
    );

    @GET("MobileApp/get_tenant_tickets")
    Call<TicketResponse> getTenantTickets(@Header("token") String token,
                                          @Header("User-ID") String user_id,
                                          @Query("acc_no") String account_number
    );

    @GET("MobileApp/get_messages")
    Call<MessageResponse> getTenantMessages(@Header("token") String token,
                                            @Header("User-ID") String user_id,
                                            @Query("acc_no") String account_number
    );

}
