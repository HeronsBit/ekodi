package ke.co.ekodi.www.ekodi.Activities;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.Networking.Responses.Transaction;
import ke.co.ekodi.www.ekodi.Networking.Responses.TransactionResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Finance extends AppCompatActivity implements TopUpFragment.OnFinanceListener {
    BottomNavigationView bottomNavigationView;
    private ApiEndPoints apiEndPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finance);

        bottomNavigationView = findViewById(R.id.bottomNav);
        bottomNavigationView.setSelectedItemId(R.id.navigation_finance);

        apiEndPoints = ApiUtils.getConfig();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_dash:
                        Intent dashIntent = new Intent(Finance.this, Dashboard.class);
//                dashIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                        finish();
                        startActivity(dashIntent);
                        overridePendingTransition(R.anim.fromleft,R.anim.fromleft);
                        return true;
                    case R.id.navigation_interactions:
                        Intent finIntent = new Intent(Finance.this, Interactions.class);
                        finIntent.putExtra("fragmentTo", R.layout.fragment_interactions);
                        finish();
                        startActivity(finIntent);
                        overridePendingTransition(R.anim.fromright,R.anim.fromleft);
                        return true;
                    case R.id.navigation_profile:
                        Intent intent = new Intent(Finance.this, Profile.class);
                        finish();
                        startActivity(intent);
                        overridePendingTransition(R.anim.fromright, R.anim.fromleft);
                        return  true;
                    default:
                        return true;
                }

            }
        });

        if (findViewById(R.id.finance_container) != null) {

            if (savedInstanceState != null) {
                return;
            }

            int intentFragment = getIntent().getExtras().getInt("fragmentTo");

            switch (intentFragment) {
                case R.layout.fragment_finance:
                    Toolbar toolbar = findViewById(R.id.app_bar);
                    setSupportActionBar(toolbar);

//                getSupportActionBar().setTitle(null);


                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        getSupportActionBar().setDisplayShowHomeEnabled(false);
                        getSupportActionBar().setTitle("BILLING");
                        getSupportActionBar().setSubtitle("Account Balance:  KSh. "+TokenPrefUtil.getAccountBalance(getApplicationContext()));
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.finance_container, new FinanceFragment()).commit();
                    break;
                case R.layout.fragment_interactions:
                    Toolbar toolbar2 = findViewById(R.id.app_bar);
                    setSupportActionBar(toolbar2);

//                getSupportActionBar().setTitle(null);


                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        getSupportActionBar().setDisplayShowHomeEnabled(false);
                        getSupportActionBar().setTitle("INTERACTIONS");
                        getSupportActionBar().setSubtitle("Messages & Interactions");
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.finance_container, new InteractionsFragment()).commit();
                    break;
            }
        }
    }


    @Override
    public void showTopUpInfo(View v) {
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("TOP UP");
            getSupportActionBar().setSubtitle("Deposit into the wallet");
        }
        bottomNavigationView = findViewById(R.id.bottomNav);
        hideBottomNavigationView(bottomNavigationView);

        getSupportFragmentManager().beginTransaction().replace(R.id.finConst, new TopUpFragment()).commit();
//        overridePendingTransition(R.anim.fromright,R.anim.fromleft);

    }

    @Override
    public void showBills(View view) {

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("MY INVOICES");
            getSupportActionBar().setSubtitle("Account Balance:  KSh. "+TokenPrefUtil.getAccountBalance(getApplicationContext()));
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.finance_container, new BillFragment()).commit();


    }

    @Override
    public void showTransactions(View view) {
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("MY TRANSACTIONS");
            getSupportActionBar().setSubtitle("Account Balance:  KSh. "+TokenPrefUtil.getAccountBalance(getApplicationContext()));
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.finance_container, new TransactionsFragment()).commit();

    }

    private void hideBottomNavigationView(BottomNavigationView view) {
        view.animate().translationY(view.getHeight());
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, Finance.class);
                intent.putExtra("fragmentTo", R.layout.fragment_finance);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.fromright,R.anim.fromright);
                return true;
            case R.id.navigation_dash:
                Intent dashIntent = new Intent(this, Dashboard.class);
//                dashIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                finish();
                startActivity(dashIntent);
                return true;
            case R.id.navigation_finance:
                Intent finIntent = new Intent(this, Finance.class);
                finIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                finish();
                startActivity(finIntent);
                return true;
            case R.id.navigation_interactions:
                Intent interIntent = new Intent(this, Interactions.class);
                interIntent.putExtra("fragmentTo", R.layout.fragment_interactions);
                finish();
                startActivity(interIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}