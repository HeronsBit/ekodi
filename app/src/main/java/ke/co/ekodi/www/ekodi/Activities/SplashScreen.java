package ke.co.ekodi.www.ekodi.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import ke.co.ekodi.www.ekodi.R;

public class SplashScreen extends AppCompatActivity {

    ImageView imageTop,imageBottom;
    TextView textView;
    Animation fromtop,frombottom,fromleft;
    private final int DISPLAY_TIME =1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        textView = findViewById(R.id.splashText);

        Typeface myFont = Typeface.createFromAsset(getAssets(),"fonts/Seravek-Bold.ttf");

//        textView.setTypeface(myFont);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                Intent mainIntent = new Intent(SplashScreen.this,Welcome1.class);
                SplashScreen.this.startActivity(mainIntent);
                SplashScreen.this.finish();
            }
        }, DISPLAY_TIME);


    }
}
