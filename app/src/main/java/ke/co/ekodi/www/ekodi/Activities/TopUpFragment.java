package ke.co.ekodi.www.ekodi.Activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopUpFragment extends Fragment {

    TextView account;
    TextView house;
    TextView rent;
    TextView paybill;
    TextView unit;
    TextView instruction3;
    TextView instruction4;

    public  interface  OnFinanceListener{
        public void showTopUpInfo(View v);
        public  void showBills(View view);
        public  void showTransactions(View view);
    }


    public TopUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_top_up, container, false);

        account = view.findViewById(R.id.account_number_topup);
        house = view.findViewById(R.id.house_name_topup);
        rent = view.findViewById(R.id.rent_topup);
        paybill = view.findViewById(R.id.paybill_text_topup);
        unit = view.findViewById(R.id.unit_text_topup);
        instruction3 = view.findViewById(R.id.text_instr3);
        instruction4 = view.findViewById(R.id.text_instr4);
        String acc = TokenPrefUtil.getUserId(getContext());
        String rentText = "KSh. "+TokenPrefUtil.getRent(getContext());
        String instr3 = "3. Paybill and enter "+TokenPrefUtil.getPayBillNumber(getContext());
        String instr4 = "4. Account "+TokenPrefUtil.getUserId(getContext());

        account.setText(acc);
        house.setText(TokenPrefUtil.getHouse(getContext()));
        rent.setText(rentText);
        paybill.setText(TokenPrefUtil.getPayBillNumber(getContext()));
        unit.setText(TokenPrefUtil.getUnit(getContext()));
        instruction3.setText(instr3);
        instruction4.setText(instr4);
        return view;
    }

}
