package ke.co.ekodi.www.ekodi.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ke.co.ekodi.www.ekodi.Models.InvoiceModel;
import ke.co.ekodi.www.ekodi.R;

public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {

    private List<InvoiceModel> invoiceData;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView title,date,amount;
        View layout;
        public MyViewHolder(View v) {
            super(v);
            layout = v;
            title = v.findViewById(R.id.invoice_title);
            date = v.findViewById(R.id.invoice_sub_title);
            amount = v.findViewById(R.id.invoice_amount);
        }
    }

    public void add(int position, InvoiceModel item) {
        invoiceData.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        invoiceData.remove(position);
        notifyItemRemoved(position);
    }

    public InvoiceAdapter(List<InvoiceModel> myDataset) {
        invoiceData = myDataset;
    }



    @NonNull
    @Override
    public InvoiceAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater inflater = LayoutInflater.from(
                viewGroup.getContext());
        View v = inflater.inflate(R.layout.fragment_single_bill, viewGroup, false);

        InvoiceAdapter.MyViewHolder vh = new InvoiceAdapter.MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull InvoiceAdapter.MyViewHolder myViewHolder, int i) {

        final String invTitle = invoiceData.get(i).getTitle();
        final String invsDate = invoiceData.get(i).getDate();
        final String invAmount = invoiceData.get(i).getAmount();
        myViewHolder.title.setText(invTitle);
        myViewHolder.date.setText(invsDate);
        myViewHolder.amount.setText(invAmount);

    }

    @Override
    public int getItemCount() {
        return invoiceData.size();
    }
}
