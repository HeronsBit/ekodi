package ke.co.ekodi.www.ekodi.Models;

public class MessageModel {

    String topic;
    String date;

    String message;

    public MessageModel(String topic, String date, String message) {
        this.topic = topic;
        this.date = date;
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
