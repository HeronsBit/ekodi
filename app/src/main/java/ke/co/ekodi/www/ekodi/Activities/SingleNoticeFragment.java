package ke.co.ekodi.www.ekodi.Activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ke.co.ekodi.www.ekodi.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class SingleNoticeFragment extends Fragment {


    public SingleNoticeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_single_notice, container, false);
    }

}
