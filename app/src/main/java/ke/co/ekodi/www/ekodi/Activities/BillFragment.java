package ke.co.ekodi.www.ekodi.Activities;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Adapters.InvoiceAdapter;
import ke.co.ekodi.www.ekodi.Models.InvoiceModel;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;

public class BillFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    List<InvoiceModel> invoices;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bill, container, false);

        mRecyclerView = view.findViewById(R.id.invoice_recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<InvoiceModel> input = new ArrayList<>();

//        InvoiceModel model = new InvoiceModel("JULY-RENT","11/12/2017","KSh. 2500");
//
//        for (int i=0;i<15;i++){
//            input.add(model);
//        }

        String gettingDescription = TokenPrefUtil.getDescriptions(getContext());
//        Log.d("Descs","Yes");
        String gettingAmount = TokenPrefUtil.getInvoiceAmount(getContext());
        String gettingSubTitle = TokenPrefUtil.getSubTitle(getContext());

        String[] itemsIneed = gettingDescription.split(",");
        String[] amounts = gettingAmount.split(",");
        String[] subTitles = gettingSubTitle.split(",");
        for (int i=0;i<itemsIneed.length;i++){
            input.add(new InvoiceModel(itemsIneed[i],subTitles[i],"KSh. "+amounts[i]));
        }
        mAdapter = new InvoiceAdapter(input);
        mRecyclerView.setAdapter(mAdapter);
        // Inflate the layout for this fragment
        return view;
    }

}
