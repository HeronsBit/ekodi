package ke.co.ekodi.www.ekodi.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.R;

public class Chat extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    TextView message, date;
    Toolbar toolbar;

    private ApiEndPoints apiEndPoints;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        bottomNavigationView = findViewById(R.id.bottomNavChats);
        bottomNavigationView.setSelectedItemId(R.id.navigation_interactions);
        apiEndPoints = ApiUtils.getConfig();
        message = findViewById(R.id.ChatMessage);
        date = findViewById(R.id.ChatDate);
        toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        activity = this;

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getIntent().getStringExtra("Title"));
            getSupportActionBar().setSubtitle(getIntent().getStringExtra("date"));
        }

        message.setText(getIntent().getStringExtra("message"));
        date.setText(getIntent().getStringExtra("date"));

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_dash:
                        Intent dashIntent = new Intent(Chat.this, Dashboard.class);
//                dashIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                        finish();
                        startActivity(dashIntent);
                        overridePendingTransition(R.anim.fromright, R.anim.fromleft);
                        return true;
                    case R.id.navigation_finance:
                        Intent finIntent = new Intent(Chat.this, Finance.class);
                        finIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                        finish();
                        startActivity(finIntent);
                        overridePendingTransition(R.anim.fromright, R.anim.fromleft);
                        return true;
                    case R.id.navigation_profile:
                        Intent intent = new Intent(Chat.this, Profile.class);
                        finish();
                        startActivity(intent);
                        overridePendingTransition(R.anim.fromright, R.anim.fromleft);
                        return true;
                    default:
                        return true;
                }

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, Interactions.class);
                intent.putExtra("fragmentTo", R.layout.fragment_messages);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.fromright,R.anim.fromright);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
