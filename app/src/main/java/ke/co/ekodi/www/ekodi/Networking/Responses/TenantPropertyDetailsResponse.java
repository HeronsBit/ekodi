package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TenantPropertyDetailsResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("Tenant Property Details")
    @Expose
    private TenantPropertyDetails tenantPropertyDetails;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public TenantPropertyDetails getTenantPropertyDetails() {
        return tenantPropertyDetails;
    }

    public void setTenantPropertyDetails(TenantPropertyDetails tenantPropertyDetails) {
        this.tenantPropertyDetails = tenantPropertyDetails;
    }

}
