package ke.co.ekodi.www.ekodi.Activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Adapters.TicketAdapter;
import ke.co.ekodi.www.ekodi.Adapters.TransactionAdapter;
import ke.co.ekodi.www.ekodi.Models.TicketModel;
import ke.co.ekodi.www.ekodi.Models.TransactionModel;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class TicketsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public TicketsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_tickets, container, false);

        mRecyclerView = view.findViewById(R.id.tickets_recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<TicketModel> input = new ArrayList<>();

        String gettingTicketTitle = TokenPrefUtil.getTicketTitle(getContext());
        String gettingTicketDate = TokenPrefUtil.getTicketDate(getContext());
        String gettingTicketStatus = TokenPrefUtil.getTicketState(getContext());

        String[] titles = gettingTicketTitle.split(",");
        String[] status = gettingTicketStatus.split(",");
        String[] dates = gettingTicketDate.split(",");
        for (int i=0;i<titles.length;i++){
            input.add(new TicketModel(titles[i],dates[i],status[i]));
        }
        mAdapter = new TicketAdapter(input);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

}
