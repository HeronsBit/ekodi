package ke.co.ekodi.www.ekodi.Models;

public class InvoiceModel {
    public  String title;
    public String date;
    public String amount;

    public InvoiceModel(String title, String date, String amount) {
        this.title = title;
        this.date = date;
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
