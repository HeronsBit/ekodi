package ke.co.ekodi.www.ekodi.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.MessageResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.InvoiceItem;
import ke.co.ekodi.www.ekodi.Networking.Responses.Invoices;
import ke.co.ekodi.www.ekodi.Networking.Responses.LogoutResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.Message;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantInvoicesResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantPropertyDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.Ticket;
import ke.co.ekodi.www.ekodi.Networking.Responses.TicketResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TopUpDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.Transaction;
import ke.co.ekodi.www.ekodi.Networking.Responses.TransactionResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.GenMessages;
import ke.co.ekodi.www.ekodi.Utilities.PreferenceManager;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity implements LoginFragment.OnLoginFormActivityListener {

    static PreferenceManager preferenceManager;
    private ApiEndPoints apiService;
    private ApiEndPoints apiService1;
    private ApiEndPoints apiService2;
    private ApiEndPoints apiService3;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        preferenceManager = new PreferenceManager(this);
        apiService = ApiUtils.getConfig();

        apiService1 = ApiUtils.getConfig();
        apiService2 = ApiUtils.getConfig();
        apiService3 = ApiUtils.getConfig();
        activity = this;

        if (findViewById(R.id.fragmentContainerDash) != null) {

            if (savedInstanceState != null) {

                return;
            }


            if (preferenceManager.getLoginStatus()) {
                Toolbar toolbar = findViewById(R.id.app_bar);
                setSupportActionBar(toolbar);
                String subtitle = TokenPrefUtil.getName(getApplicationContext());
                getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainerDash, new DashFragment()).commitAllowingStateLoss();
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                    getSupportActionBar().setDisplayShowHomeEnabled(false);
                    getSupportActionBar().setTitle("EKODI");
                    getSupportActionBar().setSubtitle(subtitle);
                    this.customizeToolbar(toolbar);
                }

            } else {

                Toolbar toolbar = findViewById(R.id.app_bar);
                setSupportActionBar(toolbar);

                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                    getSupportActionBar().setTitle(null);
                }

                getSupportFragmentManager().beginTransaction().add(R.id.fragmentContainerDash, new LoginFragment()).commit();

            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.genmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                // todo: goto back activity from here
                performLogout();
                finish();
                return true;
            case R.id.home:
                // todo: goto back activity from here
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void passResetCancel() {

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(null);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerDash, new LoginFragment()).commit();


    }

    @Override
    public void doLogin() {
        String name = TokenPrefUtil.getName(getApplicationContext());
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerDash, new DashFragment()).commitAllowingStateLoss();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle("EKODI");
            if (name == null) {
                getSupportActionBar().setSubtitle("Welcome");
            } else {
                getSupportActionBar().setSubtitle(name);
            }
            this.customizeToolbar(toolbar);
        }

    }

    @Override
    public void doPassReset() {

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(null);
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerDash, new ResetPassword()).addToBackStack(null).commit();

    }

    @Override
    public void doNetwokingStuff() {
        storeTenantDetails();
        storeTenantPropertyDetails();
        storeTopupDetails();
        storeInvoiceDetails();
        storeTenantTickets();
        storeTenantChatMessages();
        storeTransactions();
    }

    @Override
    public void refreshDashboard() {
        String name = TokenPrefUtil.getName(getApplicationContext());
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentContainerDash, new DashFragment()).commitAllowingStateLoss();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle("EKODI");
            if (name == null) {
                getSupportActionBar().setSubtitle("Welcome");
            } else {
                getSupportActionBar().setSubtitle(name);
            }
            this.customizeToolbar(toolbar);
        }
    }

    public void doWelcome() {
        Intent intent = new Intent(Dashboard.this, Welcome.class);
        startActivity(intent);
//        finish();
        overridePendingTransition(R.anim.fromright, R.anim.fromleft);
    }


    public void financeHandler(View v) {
        Intent financer = new Intent(Dashboard.this, Finance.class);
        financer.putExtra("fragmentTo", R.layout.fragment_finance);
        startActivity(financer);
        finish();
        overridePendingTransition(R.anim.fromleft, R.anim.fromleft);
    }

    public void interactionsHandler(View v) {
        Intent financer = new Intent(Dashboard.this, Interactions.class);
        financer.putExtra("fragmentTo", R.layout.fragment_interactions);
        startActivity(financer);
        finish();
        overridePendingTransition(R.anim.fromleft, R.anim.fromleft);
    }

    public void refreshFragment() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.dash_fragment);
        if (currentFragment instanceof DashFragment) {
            FragmentTransaction fragTransaction = (getSupportFragmentManager().beginTransaction());
            fragTransaction.detach(currentFragment);
            fragTransaction.attach(currentFragment);
            fragTransaction.commit();
        }
    }


    public void performLogout(){
        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());

        apiService.logoutTenant(token,user_id).enqueue(new Callback<LogoutResponse>() {
            @Override
            public void onResponse(Call<LogoutResponse> call, Response<LogoutResponse> response) {
                try{
                    assert response.body() != null;
                    if (response.body().getStatus() == 200){
                        TokenPrefUtil.clearAll();
                        preferenceManager.setLoginStatus(false);
                        doWelcome();
                        finish();
                    }

                }catch (Exception e){
                        e.printStackTrace();
//                        GenMessages.makeToast(activity,getApplicationContext(),"Logout error");
                }
            }

            @Override
            public void onFailure(Call<LogoutResponse> call, Throwable t) {

                t.printStackTrace();
                Toast.makeText(getApplicationContext(),"No internet connection",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void storeTenantDetails(){

        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());
        String acc_no = TokenPrefUtil.getUserId(getApplicationContext());

        apiService.getTenantDetails(token,user_id,acc_no).enqueue(new Callback<TenantDetailsResponse>() {
            @Override
            public void onResponse(Call<TenantDetailsResponse> call, Response<TenantDetailsResponse> response) {

               try{
                   assert response.body() != null;
                   if (response.body().getStatus()==200){
//                       Toast.makeText(getApplicationContext(),"Yes user details for you",Toast.LENGTH_LONG).show();
                       List<Message>messageList=response.body().getMessage();
                       String name = messageList.get(0).getFullNames();
                       String email = messageList.get(0).getEmail();
                       String phone = messageList.get(0).getPhoneNumber();
                       String dob = messageList.get(0).getDob();
                       TokenPrefUtil.storeName(getApplicationContext(),name);
                       TokenPrefUtil.storeEmail(getApplicationContext(),email);
                       TokenPrefUtil.storePhone(getApplicationContext(),phone);
                       TokenPrefUtil.storeDob(getApplicationContext(),dob);
                       Log.d("NAME",name);
                   }else {
                       GenMessages.makeToast(activity,getApplicationContext(),"Problem loading your details");
                   }

               }catch (Exception e){
                   e.printStackTrace();
               }

            }

            @Override
            public void onFailure(Call<TenantDetailsResponse> call, Throwable t) {
                t.printStackTrace();
                GenMessages.makeToast(activity,getApplicationContext(),"Unknown Error");

            }
        });

    }
    public void storeTenantPropertyDetails(){
        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());
        String acc_no = TokenPrefUtil.getUserId(getApplicationContext());

        apiService1.getTenantPropertyDetails(token,user_id,acc_no).enqueue(new Callback<TenantPropertyDetailsResponse>() {
            @Override
            public void onResponse(Call<TenantPropertyDetailsResponse> call, Response<TenantPropertyDetailsResponse> response) {
             try {
                 assert response.body() != null;
                 if (response.body().getStatus() == 200){
                     String house = response.body().getTenantPropertyDetails().getUnitName();
                     String rent = response.body().getTenantPropertyDetails().getUnitRent();
                     String building = response.body().getTenantPropertyDetails().getBuildingName();

                     TokenPrefUtil.storeHouse(getApplicationContext(),house);
                     TokenPrefUtil.storeRent(getApplicationContext(),rent);
                     TokenPrefUtil.storeBuilding(getApplicationContext(),building);

                     Log.d("HOUSE",house);

                 }
             }catch (Exception e){
                 e.printStackTrace();
             }
            }

            @Override
            public void onFailure(Call<TenantPropertyDetailsResponse> call, Throwable t) {

            }
        });
    }

    public void storeTopupDetails(){
        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());
        String acc_no = TokenPrefUtil.getUserId(getApplicationContext());

        apiService2.getTopUpDetails(token,user_id,acc_no).enqueue(new Callback<TopUpDetailsResponse>() {
            @Override
            public void onResponse(Call<TopUpDetailsResponse> call, Response<TopUpDetailsResponse> response) {
                try {
                    assert response.body() !=null;
                    if (response.body().getStatus() == 200){
                        TokenPrefUtil.storePayBillNumber(getApplicationContext(),response.body().getTopUpDetails().getMpesaPaybill());
                        Log.d("PAYBILL",TokenPrefUtil.getPayBillNumber(getApplicationContext()));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TopUpDetailsResponse> call, Throwable t) {

            }
        });
    }

    public void storeInvoiceDetails(){
        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());
        String acc_no = TokenPrefUtil.getUserId(getApplicationContext());

        apiService3.getTenantInvoices(token,user_id,acc_no).enqueue(new Callback<TenantInvoicesResponse>() {
            @Override
            public void onResponse(Call<TenantInvoicesResponse> call, Response<TenantInvoicesResponse> response) {
           try {
               assert response.body() !=null;
               if (response.body().getStatus() == 200){

                   //TODO checking nullability

                   List<Invoices> inv = response.body().getInvoices();
                   TokenPrefUtil.storeUnit(getApplicationContext(),inv.get(0).getPropertyUnit());
                   String unit =inv.get(0).getPropertyUnit();
                   Log.d("UNIT",unit);

                   List<InvoiceItem> invoices = inv.get(0).getInvoiceItems();
                   List<String> descs = new ArrayList<>();
                   List<String> subTitle = new ArrayList<>();
                   List<String> amount = new ArrayList<>();
                   for (int i=0;i<invoices.size();i++){
                       String desc = invoices.get(i).getItem();
                       String titleSub = invoices.get(i).getInvoiceNumber();
                       String amnt = invoices.get(i).getAmount();
                       descs.add(desc);
                       subTitle.add(titleSub);
                       amount.add(amnt);
                       Log.d("DESCRIPTION "+i,desc);

                   }

                   TokenPrefUtil.storeDescriptions(descs);
                   TokenPrefUtil.storeInvoiceAmount(amount);
                   TokenPrefUtil.storeSubTitle(subTitle);

               }
           }catch (Exception e){
               e.printStackTrace();
           }
            }

            @Override
            public void onFailure(Call<TenantInvoicesResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(getApplicationContext(),"Invoices failing",Toast.LENGTH_LONG).show();
            }
        });
    }

    public void storeTenantTickets(){
        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());
        String acc_no = TokenPrefUtil.getUserId(getApplicationContext());

        apiService.getTenantTickets(token,user_id,acc_no).enqueue(new Callback<TicketResponse>() {
            @Override
            public void onResponse(Call<TicketResponse> call, Response<TicketResponse> response) {
                try {
                    assert response.body() !=null;
                    if (response.body().getStatus() == 200){

                        List<Ticket> myTickets = response.body().getTickets();
                        List<String> status = new ArrayList<>();
                        List<String> date = new ArrayList<>();
                        List<String> title = new ArrayList<>();
                        for (int i=0;i<myTickets.size();i++){
                            String state = myTickets.get(i).getStatus();
                            String dt = myTickets.get(i).getDate();
                            String titl = myTickets.get(i).getTitle();
                            status.add(state);
                            date.add(dt);
                            title.add(titl);
                            Log.d("State",state);
                            Log.d("Title",titl);
                            Log.d("Date",dt);

                        }

                        TokenPrefUtil.storeTicketTitle(title);
                        TokenPrefUtil.storeTicketDate(date);
                        TokenPrefUtil.storeTicketState(status);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TicketResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }

    public void storeTenantChatMessages(){
        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());
        String acc_no = TokenPrefUtil.getUserId(getApplicationContext());

        apiService.getTenantMessages(token,user_id,acc_no).enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                try {
                    assert response.body() !=null;
                    if (response.body().getStatus() == 200){
                        List<ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.Message> myMessages = response.body().getMessage();
                        List<String> message = new ArrayList<>();
                        List<String> date = new ArrayList<>();

                        for (int i=0;i<myMessages.size();i++){
                            String msg = myMessages.get(i).getMessage();
                            String dt = myMessages.get(i).getDate();
                            message.add(msg);
                            date.add(dt);
                            Log.d("Message",msg);
                            Log.d("Date",dt);

                        }

                        TokenPrefUtil.storeTenantChatMessage(message);
                        TokenPrefUtil.storeTenantChatDate(date);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }

    public void storeTransactions(){
        String token = TokenPrefUtil.getApiKey(getApplicationContext());
        String user_id = TokenPrefUtil.getUserId(getApplicationContext());
        String acc_no = TokenPrefUtil.getUserId(getApplicationContext());

        apiService.getTenantTransactions(token,user_id,acc_no).enqueue(new Callback<TransactionResponse>() {
            @Override
            public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                try {
                    assert response.body() !=null;
                    if (response.body().getStatus() == 200){


                        List<Transaction> transactions = response.body().getTransactions();
                        TokenPrefUtil.storeAccountBalance(getApplicationContext(),transactions.get(0).getWallet().getAmount());
                        List<String> amount = new ArrayList<>();
                        List<String> date = new ArrayList<>();
                        List<String> title = new ArrayList<>();
                        for (int i=0;i<transactions.size();i++){
                            String amnt = transactions.get(i).getTransAmount();
                            String dt = transactions.get(i).getTransDate();
                            String titl = transactions.get(i).getTransID();
                            amount.add(amnt);
                            date.add(dt);
                            title.add(titl);
                            Log.d("Transaction",amnt);

                        }

                        TokenPrefUtil.storeTransactionAmount(amount);
                        TokenPrefUtil.storeTransactionDate(date);
                        TokenPrefUtil.storeTransactionTitle(title);

                        Log.d("TRANSTITLE",TokenPrefUtil.getTransactionTitle(getApplicationContext()));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TransactionResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }






    //TODO customizing toolbars
    public void customizeToolbar(Toolbar toolbar){
        // Save current title and subtitle
        final CharSequence originalTitle = toolbar.getTitle();
        final CharSequence originalSubtitle = toolbar.getSubtitle();

        // Temporarily modify title and subtitle to help detecting each
        toolbar.setTitle("title");
        toolbar.setSubtitle("subtitle");

        for(int i = 0; i < toolbar.getChildCount(); i++){
            View view = toolbar.getChildAt(i);

            if(view instanceof TextView){
                TextView textView = (TextView) view;


                if(textView.getText().equals("title")){
                    // Customize title's TextView
//                    Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
////                    params.gravity = Gravity.START;
//                    params.leftMargin = 20;
//                    textView.setLayoutParams(params);

                    // Apply custom font using the Calligraphy library
//                    Typeface typeface = TypefaceUtils.load(getAssets(), "fonts/myfont-1.otf");
//                    textView.setTypeface(typeface);

                } else if(textView.getText().equals("subtitle")){
                    // Customize subtitle's TextView
                    Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
                    params.gravity = Gravity.END;
                    params.setMargins(0,-10,20,0);
                    textView.setLayoutParams(params);

                    // Apply custom font using the Calligraphy library
//                    Typeface typeface = TypefaceUtils.load(getAssets(), "fonts/myfont-2.otf");
//                    textView.setTypeface(typeface);
                }
            }
        }

        // Restore title and subtitle
        toolbar.setTitle(originalTitle);
        toolbar.setSubtitle(originalSubtitle);
    }
    public void customizeToolbarTitle(Toolbar toolbar){

        final CharSequence originalTitle = toolbar.getTitle();
        toolbar.setTitle("title");

        for(int i = 0; i < toolbar.getChildCount(); i++){
            View view = toolbar.getChildAt(i);

            if(view instanceof TextView){
                TextView textView = (TextView) view;

                if(textView.getText().equals("title")) {
                    // Customize title's TextView
                    Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
                    params.gravity = Gravity.END;
                    textView.setLayoutParams(params);

                    // Apply custom font using the Calligraphy library
//                    Typeface typeface = TypefaceUtils.load(getAssets(), "fonts/myfont-1.otf");
//                    textView.setTypeface(typeface);
                }

            }
        }
        toolbar.setTitle(originalTitle);

    }

    public void customizeToolbarSubTitle(Toolbar toolbar){

        final CharSequence originalSubTitle = toolbar.getSubtitle();
        toolbar.setSubtitle("subTitle");

        for(int i = 0; i < toolbar.getChildCount(); i++){
            View view = toolbar.getChildAt(i);

            if(view instanceof TextView){
                TextView textView = (TextView) view;

                // Customize title's TextView
                if(textView.getText().equals("subTitle")) {
                    Toolbar.LayoutParams params = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.MATCH_PARENT);
                    params.gravity = Gravity.END;
                    textView.setLayoutParams(params);

                    // Apply custom font using the Calligraphy library
//                    Typeface typeface = TypefaceUtils.load(getAssets(), "fonts/myfont-1.otf");
//                    textView.setTypeface(typeface);
                }

            }
        }
        toolbar.setTitle(originalSubTitle);


    }



}
