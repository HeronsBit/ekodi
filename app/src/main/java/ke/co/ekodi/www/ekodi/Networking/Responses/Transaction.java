package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Transaction {

    @SerializedName("property name")
    @Expose
    private String propertyName;
    @SerializedName("property unit")
    @Expose
    private String propertyUnit;
    @SerializedName("wallet")
    @Expose
    private Wallet wallet;
    @SerializedName("Trans ID mpesa")
    @Expose
    private Object transIDMpesa;
    @SerializedName("Trans amount")
    @Expose
    private String transAmount;
    @SerializedName("Confirmed if bank")
    @Expose
    private String confirmedIfBank;
    @SerializedName("Slip Id if bank")
    @Expose
    private Object slipIdIfBank;
    @SerializedName("trans ID")
    @Expose
    private String transID;
    @SerializedName("Account number")
    @Expose
    private Object accountNumber;
    @SerializedName("Trans date")
    @Expose
    private String transDate;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyUnit() {
        return propertyUnit;
    }

    public void setPropertyUnit(String propertyUnit) {
        this.propertyUnit = propertyUnit;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public Object getTransIDMpesa() {
        return transIDMpesa;
    }

    public void setTransIDMpesa(Object transIDMpesa) {
        this.transIDMpesa = transIDMpesa;
    }

    public String getTransAmount() {
        return transAmount;
    }

    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    public String getConfirmedIfBank() {
        return confirmedIfBank;
    }

    public void setConfirmedIfBank(String confirmedIfBank) {
        this.confirmedIfBank = confirmedIfBank;
    }

    public Object getSlipIdIfBank() {
        return slipIdIfBank;
    }

    public void setSlipIdIfBank(Object slipIdIfBank) {
        this.slipIdIfBank = slipIdIfBank;
    }

    public String getTransID() {
        return transID;
    }

    public void setTransID(String transID) {
        this.transID = transID;
    }

    public Object getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Object accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

}
