package ke.co.ekodi.www.ekodi.Networking.Responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Invoices {

    @SerializedName("property name")
    @Expose
    private String propertyName;
    @SerializedName("property unit")
    @Expose
    private String propertyUnit;
    @SerializedName("invoice NO")
    @Expose
    private String invoiceNO;
    @SerializedName("invoice amount")
    @Expose
    private String invoiceAmount;
    @SerializedName("invoice_balance")
    @Expose
    private String invoiceBalance;
    @SerializedName("invoice items")
    @Expose
    private List<InvoiceItem> invoiceItems = null;
    @SerializedName("invoice date")
    @Expose
    private String invoiceDate;
    @SerializedName("invoice date due")
    @Expose
    private String invoiceDateDue;
    @SerializedName("invoice status")
    @Expose
    private String invoiceStatus;

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyUnit() {
        return propertyUnit;
    }

    public void setPropertyUnit(String propertyUnit) {
        this.propertyUnit = propertyUnit;
    }

    public String getInvoiceNO() {
        return invoiceNO;
    }

    public void setInvoiceNO(String invoiceNO) {
        this.invoiceNO = invoiceNO;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getInvoiceBalance() {
        return invoiceBalance;
    }

    public void setInvoiceBalance(String invoiceBalance) {
        this.invoiceBalance = invoiceBalance;
    }

    public List<InvoiceItem> getInvoiceItems() {
        return invoiceItems;
    }

    public void setInvoiceItems(List<InvoiceItem> invoiceItems) {
        this.invoiceItems = invoiceItems;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceDateDue() {
        return invoiceDateDue;
    }

    public void setInvoiceDateDue(String invoiceDateDue) {
        this.invoiceDateDue = invoiceDateDue;
    }

    public String getInvoiceStatus() {
        return invoiceStatus;
    }

    public void setInvoiceStatus(String invoiceStatus) {
        this.invoiceStatus = invoiceStatus;
    }

}
