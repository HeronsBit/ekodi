package ke.co.ekodi.www.ekodi.Activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.Networking.Responses.Message;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantDetailsResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashFragment extends Fragment {

    TextView account;
    TextView house;
    TextView rent;
    TextView paybill;
    TextView unit;
    TextView accBal;
    CardView financeCard;
    ProgressBar reload;
    LoginFragment.OnLoginFormActivityListener onLoginFormActivityListener;
//    Dashboard dash;
    private ApiEndPoints apiService;


    public  interface  OnFinanceActivityListener{
        public void showTopUp();
        public  void showBills();
        public  void showTransactions();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dash, container, false);

        apiService=ApiUtils.getConfig();

//        storeTenantDetails();

        reload = view.findViewById(R.id.progressReload);
        reload.setVisibility(View.GONE);
        account = view.findViewById(R.id.account_number_dash);
        house = view.findViewById(R.id.house_name_dash);
        rent = view.findViewById(R.id.rent_dash);
        paybill = view.findViewById(R.id.paybill_text_dash);
        unit = view.findViewById(R.id.unit_text_dash);
        accBal = view.findViewById(R.id.text_account_balance);
        String acc = TokenPrefUtil.getUserId(getContext());
        String rentText = "KSh. "+TokenPrefUtil.getRent(getContext());
        String hse = TokenPrefUtil.getHouse(getContext());
        String pbill = TokenPrefUtil.getPayBillNumber(getContext());
        String unt = TokenPrefUtil.getUnit(getContext());
        String bal = TokenPrefUtil.getAccountBalance(getContext())+"/=";
        account.setText(acc);
        if (rentText.equals("")){
            rent.setText(R.string.rent_val);
        }else {
            rent.setText(rentText);
        }
        if (bal.equals(null)){
            accBal.setText(R.string.zero);
        }else {
            accBal.setText(bal);
        }
        if (hse == null){
            house.setText(R.string.my_house);
        }else {
            house.setText(hse);
        }
        if (pbill == null){
            paybill.setText(R.string.paybill);
        }else {
            paybill.setText(pbill);
        }
        if (unt == null){
            unit.setText(R.string.my_unit);
        }else {
            unit.setText(unt);
        }
//        Log.d("UNIT ANOTHER",TokenPrefUtil.getUnit(getContext()));


        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            // Refresh your fragment here
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            Log.i("IsRefresh", "Yes");
        }
    }

}
