package ke.co.ekodi.www.ekodi.Activities;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.ApiUtils;
import ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.MessageResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.InvoiceItem;
import ke.co.ekodi.www.ekodi.Networking.Responses.Invoices;
import ke.co.ekodi.www.ekodi.Networking.Responses.Message;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantInvoicesResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TenantPropertyDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.Ticket;
import ke.co.ekodi.www.ekodi.Networking.Responses.TicketResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.TopUpDetailsResponse;
import ke.co.ekodi.www.ekodi.Networking.Responses.Transaction;
import ke.co.ekodi.www.ekodi.Networking.Responses.TransactionResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.GenMessages;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Interactions extends AppCompatActivity implements InteractionsFragment.onInteractionsFragmentListener{
    BottomNavigationView bottomNavigationView;
    private ApiEndPoints apiEndPoints;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interactions);

        bottomNavigationView = findViewById(R.id.bottomNavInteractions);
        bottomNavigationView.setSelectedItemId(R.id.navigation_interactions);
        apiEndPoints= ApiUtils.getConfig();
        activity = this;


        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.navigation_dash:
                        Intent dashIntent = new Intent(Interactions.this, Dashboard.class);
//                dashIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                        finish();
                        startActivity(dashIntent);
                        overridePendingTransition(R.anim.fromright,R.anim.fromleft);
                        return true;
                    case R.id.navigation_finance:
                        Intent finIntent = new Intent(Interactions.this, Finance.class);
                        finIntent.putExtra("fragmentTo", R.layout.fragment_finance);
                        finish();
                        startActivity(finIntent);
                        overridePendingTransition(R.anim.fromright,R.anim.fromleft);
                        return true;
                    case R.id.navigation_profile:
                        Intent intent = new Intent(Interactions.this, Profile.class);
                        finish();
                        startActivity(intent);
                        overridePendingTransition(R.anim.fromright, R.anim.fromleft);
                        return  true;
                    default:
                        return true;
                }

            }
        });

        if (findViewById(R.id.interactions_container) != null) {

            if (savedInstanceState != null) {
                return;
            }

            int intentFragment = getIntent().getExtras().getInt("fragmentTo");

            switch (intentFragment) {
                case R.layout.fragment_interactions:
                    Toolbar toolbar2 = findViewById(R.id.app_bar);
                    setSupportActionBar(toolbar2);

                    if (getSupportActionBar() != null) {

                        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        getSupportActionBar().setDisplayShowHomeEnabled(false);
                        getSupportActionBar().setTitle("INTERACTIONS");
                        getSupportActionBar().setSubtitle("Messages & Interactions");
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new InteractionsFragment()).commit();
                    break;
                case R.layout.fragment_messages:
                    Toolbar toolbar3 = findViewById(R.id.app_bar);
                    setSupportActionBar(toolbar3);

                    if (getSupportActionBar() != null) {

                        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                        getSupportActionBar().setDisplayShowHomeEnabled(false);
                        getSupportActionBar().setTitle("MY MESSAGES");
                        getSupportActionBar().setSubtitle("Direct Messages");
                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new MessagesFragment()).commit();
                    break;
            }
        }
    }

    @Override
    public void showNoticeBoard(View view) {

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("NOTICEBOARD");
            getSupportActionBar().setSubtitle("House Information");
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new NoticeboardFragment()).commit();

    }

    @Override
    public void loadTickets(View view) {

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("MY TICKETS");
            getSupportActionBar().setSubtitle("Support & Maintenance Tickets");
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new TicketsFragment()).commit();

    }

    @Override
    public void loadMessages(View view) {

        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("MY MESSAGES");
            getSupportActionBar().setSubtitle("Direct Messages");
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new MessagesFragment()).commit();

    }

    @Override
    public void viewChats(View view) {
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("MESSAGE 3");
            getSupportActionBar().setSubtitle("20/08/2018");
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new SingleMessageFragment()).commit();

    }

    @Override
    public void ticketChats(View view) {
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("TOPIC 1");
            getSupportActionBar().setSubtitle("20/08/2018");
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new SingleTicketFragment()).commit();

    }

    @Override
    public void loadTicketAdder(View view) {
        Toolbar toolbar = findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle("TICKET");
            getSupportActionBar().setSubtitle("Add Ticket");
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.interactions_container, new AddTicketFragment()).commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, Interactions.class);
                intent.putExtra("fragmentTo", R.layout.fragment_interactions);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.fromright,R.anim.fromright);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}