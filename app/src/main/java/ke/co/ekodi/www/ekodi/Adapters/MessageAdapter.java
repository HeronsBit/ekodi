package ke.co.ekodi.www.ekodi.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Activities.Chat;
import ke.co.ekodi.www.ekodi.Activities.Interactions;
import ke.co.ekodi.www.ekodi.Models.InvoiceModel;
import ke.co.ekodi.www.ekodi.Models.MessageModel;
import ke.co.ekodi.www.ekodi.Networking.ApiEndPoints;
import ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.MessageResponse;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MyViewHolder> {

    private List<MessageModel> messageData;

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    Context context;
    ApiEndPoints apiService;
     class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView topic,date;
        View layout;

        List<MessageModel> msgs = new ArrayList<>();
        Context vcontext;

        public MyViewHolder(View v, Context vcontext, List<MessageModel> msgs) {
            super(v);
            this.vcontext = vcontext;
            this.msgs = msgs;
            layout = v;
            topic = v.findViewById(R.id.message_title);
            date = v.findViewById(R.id.message_date);
            v.setOnClickListener(this);
        }

         @Override
         public void onClick(View view) {

            int pos = getAdapterPosition();
            MessageModel msg = this.msgs.get(pos);
             Intent intent = new Intent(this.vcontext,Chat.class);
             intent.putExtra("Title",msg.getTopic());
             intent.putExtra("date",msg.getDate());
             intent.putExtra("message",msg.getMessage());
             this.vcontext.startActivity(intent);
             ((Activity)context).finish();

         }
     }

    public void add(int position, MessageModel item) {
        messageData.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        messageData.remove(position);
        notifyItemRemoved(position);
    }

    public MessageAdapter(List<MessageModel> messageData,Context context) {
        this.messageData = messageData;
        this.context = context;
    }

    @NonNull
    @Override
    public MessageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(
                viewGroup.getContext());
        View v = inflater.inflate(R.layout.fragment_message_row, viewGroup, false);

        MessageAdapter.MyViewHolder vh = new MessageAdapter.MyViewHolder(v,context,messageData);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.MyViewHolder myViewHolder, int i) {

        final String msgTopic = messageData.get(i).getTopic();
        final String msgDate = messageData.get(i).getDate();
        myViewHolder.topic.setText(msgTopic);
        myViewHolder.date.setText(msgDate);
    }

    @Override
    public int getItemCount() {
        return messageData.size();
    }

    public void storeTenantChatMessages(){
        String token = TokenPrefUtil.getApiKey(getContext());
        String user_id = TokenPrefUtil.getUserId(getContext());
        String acc_no = TokenPrefUtil.getUserId(getContext());

        apiService.getTenantMessages(token,user_id,acc_no).enqueue(new Callback<MessageResponse>() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                try {
                    assert response.body() !=null;
                    if (response.body().getStatus() == 200){
                        List<ke.co.ekodi.www.ekodi.Networking.Responses.ChatMessages.Message> myMessages = response.body().getMessage();
                        List<String> message = new ArrayList<>();
                        List<String> date = new ArrayList<>();

                        for (int i=0;i<myMessages.size();i++){
                            String msg = myMessages.get(i).getMessage();
                            String dt = myMessages.get(i).getDate();
                            message.add(msg);
                            date.add(dt);
                            Log.d("Message",msg);
                            Log.d("Date",dt);

                        }

                        TokenPrefUtil.storeTenantChatMessage(message);
                        TokenPrefUtil.storeTenantChatDate(date);

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });
    }
}
