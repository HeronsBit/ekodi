package ke.co.ekodi.www.ekodi.Activities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ke.co.ekodi.www.ekodi.Adapters.InvoiceAdapter;
import ke.co.ekodi.www.ekodi.Adapters.TransactionAdapter;
import ke.co.ekodi.www.ekodi.Models.InvoiceModel;
import ke.co.ekodi.www.ekodi.Models.TransactionModel;
import ke.co.ekodi.www.ekodi.R;
import ke.co.ekodi.www.ekodi.Utilities.TokenPrefUtil;


/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    List<TransactionModel> transactions;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_transactions, container, false);

        mRecyclerView = view.findViewById(R.id.transaction_recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        List<TransactionModel> input = new ArrayList<>();

        String gettingTransDate = TokenPrefUtil.getTransactionDate(getContext());
        String gettingTransAmount = TokenPrefUtil.getTransactionAmount(getContext());
        String gettingTransTitle = TokenPrefUtil.getTransactionTitle(getContext());

        String[] titles = gettingTransTitle.split(",");
        String[] amounts = gettingTransAmount.split(",");
        String[] dates = gettingTransDate.split(",");
        for (int i=0;i<titles.length;i++){
            input.add(new TransactionModel("TOP UP",dates[i],"KSh. "+amounts[i]));
        }
        mAdapter = new TransactionAdapter(input);
        mRecyclerView.setAdapter(mAdapter);
        // Inflate the layout for this fragment
        return view;
    }

}
